use Search::Elasticsearch;
use LWP::Simple;
use Encode::Byte;
use File::ShareDir::PAR;
use PAR::Packer;
use Search::Elasticsearch::Serializer::JSON;
use Search::Elasticsearch::Client::2_0::Direct;
use Search::Elasticsearch::Client::2_0::Direct::Indices;
use Search::Elasticsearch::Cxn::Factory;
use Search::Elasticsearch::CxnPool::Static;
use Search::Elasticsearch::Logger::LogAny;
use Search::Elasticsearch::Transport;
use Search::Elasticsearch::Cxn::HTTPTiny;
use Search::Elasticsearch::Scroll;
use Data::Dumper;
use Benchmark qw(:hireswallclock);

#To create EXE use cmd: pp -o {exename} -c -x -v {scriptname with location if needed}
#To create linux binary, use 10.128.70.241 server and use: /usr/local/bin/pp -o {linux binary name} -c -x -v {script name with location (if needed))

#Usually we need only Search::Elasticsearch, other packages are loaded in runtime, however it is necessary to keep all the Search::Elasticsearch::* as par package (which is used to to make linux binary and windows executable) will recognize these packages only from script and not in runtime.

@array = @ARGV;

if ($#array==-1) {			#The script must pass an argument.

	print "No Arguments Passed, please check option '--help' for more information\n";
	exit;
}

while ($_ = $ARGV[0]) {

    shift;
    #last if /^--$/;
    if ($array[0] == 1 || $array[0]==2) {			#For Full Load and Incremental Load
		if ($#array != 7) {
			print "Invalid Number of Arguments, please check option '--help' for more information\n";
			exit;
		}
		$load_type=$array[0];
		$host = $array[1];
		$port = $array[2];
		$primary_index = $array[3];
		$primary_field = $array[4];
		$secondary_index = $array[5];
		$secondary_field = $array[6];
		$merge_index = $array[7];
	}
	elsif ($array[0] == 3) {					#For Loading the data with Timestamp
		if ($#array != 11) {
			print "Invalid Number of Arguments, please check option '--help' for more information\n";
			exit;
		}
		$load_type=$array[0];
		$host = $array[1];
		$port = $array[2];
		$primary_index = $array[3];
		$primary_field = $array[4];
		$primary_from_ts = $array[5];
		$primary_to_ts = $array[6];
		$secondary_index = $array[7];
		$secondary_field = $array[8];
		$secondary_from_ts = $array[9];
		$secondary_to_ts = $array[10];
		$merge_index = $array[11];
	}
	elsif (/^-dev/) {
		$DEV="true";
	}
	elsif (/^-manual/) {
		$MANUAL="true";
	}
	elsif (/^--help/) {			#For Loading Help content, check the formart part at the end of the script.
		$~="HELP";
		write;
		exit;
	}
	else {
		print "Invalid Arguments Passed, please check option '--help' for more information\n";
		exit;
	}
}

sub CheckES {		#To check if ES is running on given host and port or not, it uses LWP::Simple
	$host=shift;
	$port=shift;
	$url = "http://$host:$port/";
	$data = get($url);
	if (defined $data) {
		return true;
	}
	else {
		print "ES is not running on host: $host and port:$port\n";
		exit;
	}
}

sub CheckIfIndexExist {		#To check if index exist or not -- using LWP::Simple.

	$index = shift;
	$key=shift; #If 0, then index is mandatory. If 1, then index will be created if not exit and return true.
	$url = "http://$host:$port/".$index."/";
	$data = get($url);
	if ( $key == 0) {
		if (defined $data) {
			return true;
		}
		else {
			print "Index $index does not exist; please check and try again.\n";
			exit;
		}
	}
	else {
		if (defined $data) {
			print "Index $index already exist\n";
			return true;
		}
		else {
			print "Index does not exist, creating index $index...\n";
			$merge="true";
			$es->indices->create(index=>$index);
			return true;
		}
	}
}

sub CheckIfFieldExist { 		#To check if field exist or not -- using LWP::Simple
	$index = shift;
	$field = shift;
	
	$url = $url = "http://$host:$port/".$index."/";
	$data = get($url);
	
	if ($data =~ /$field/) {
		return true;
	}
	else { 
		print "Field $field does not exist in $index, please check and try again\n";
		exit;
	}
}

sub CheckIfIndexEmpty {			#To check if index is empty or not -- using LWP::Simple

	$index=shift;
	$url = $url = "http://$host:$port/".$index."/_count";
	$data = get($url);
	$data =~ /\{\"count\"\:(\d+)\,/;
	$count = $1;
	if ($count == 0) {
		return true;
	}
}

sub ValidateTimeStamp {			#To Validate Time Stamp

	$timestamp=shift;
	
	if ($timestamp =~ /^(\d{4})\-(\d{2})\-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})\.\d{3}Z$/ && (($2>0 && $2<13) && $3>0 && (($3<30 && $1%4==0 && $2==2)||($3<29 && $2==2) || ($3<31 && $2=~/[469]|11/) || ($3<32 && $2=~/[13578]|10|12/)) && $4>=0 && $4<25 && $5>=0 && $5<61 && $6>=0 && $6<61)) {
		return $timestamp;
	}
	else {
		print "In Else $timestamp\n"; 
		until ($timestamp =~ /^(\d{4})\-(\d{2})\-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})\.\d{3}Z$/ && (($2>0 && $2<13) && $3>0 && (($3<30 && $1%4==0 && $2==2)||($3<29 && $2==2) || ($3<31 && $2=~/[469]|11/) || ($3<32 && $2=~/[13578]|10|12/)) && $4>=0 && $4<25 && $5>=0 && $5<61 && $6>=0 && $6<61)) {
			print "Invalid Timestamp! Please enter the timestamp in correct format: ";
			$timestamp=<>;
			chomp $timestamp;
			$timestamp =~ s/\s/T/;
			$timestamp =~ s/$/Z/;
		}
		return $timestamp;
	}
}

sub CheckIndexCount {			#To Check the count of index, if count is more than 1, do something (not decided yet).

	$load_type=shift;
	if ($load_type == 1) {
		$index=shift;
		$check_count=$es->search(
			index       => $index,
			search_type => 'count',
			body => {
				query => {
					match_all => {}
				}
			}
		);
		
		if ($check_count->{'hits'}->{'total'} > 1000000) {
			print "$index has more than 1000000 records, please try to load using timestamp\n";
			exit 1;
		}
	}
}

sub GetHashValueForIL { #For Incremental Load -- Not useful for MergeIndex2



$index=shift;
$field=shift;
$timestamp=shift;

$hash_index = $es->scroll_helper(
		index	=> $index,
		search_type => 'scan',          # important
		body 	=> {
		query => {
			"constant_score" => {
				"filter" => {
					"range" => {
					'@timestamp' => { 
						"gte" => "$timestamp"
							}
						}			
					}
				}	
			}
		}
	);
	
	while ($result = $hash_index->next) {
		$hash{$$result{'_id'}} = $$result{'_source'}{$primary_field};
	}
	
	return %hash;
}

sub GetHashValueForTL { #For Time Load		-- Not useful for MergeIndex2


$index=shift;
$field=shift;
$primary_timestamp=shift;
$secondary_timestamp=shift;

$hash_index = $es->scroll_helper(
		index	=> $index,
		search_type => 'scan',          # important
		body 	=> {
		query => {
			"constant_score" => {
				"filter" => {
					"range" => {
					'@timestamp' => { 
						"gte" => "$primary_timestamp",
						"lte" => "$secondary_timestamp"
							}
						}			
					}
				}	
			}
		}
	);
	
	while ($result = $hash_index->next) {
		$hash{$$result{'_id'}} = $$result{'_source'}{$primary_field};
	}
	
	return %hash;
}

sub MergeIndex {			#Older Merge Process.

	print "Loading Data Into Elasticsearch, please note this may take some time...\n";
	$load_type=shift;
	if($load_type==1) {
	
		$primary_index=shift;
		$primary_field=shift;
		$secondary_index=shift;
		$secondary_field=shift;
		$merge_index=shift;
		
		$hashprimary_index = $es->scroll_helper(
			index       => $primary_index,
			search_type => 'scan'          # important
		);
		
		while ($result = $hashprimary_index->next) {
			$primary_hash{$$result{'_id'}} = $$result{'_source'}{$primary_field};
		}
		%rprimary_hash = reverse %primary_hash;
	
		$hashsecondary_index = $es->scroll_helper(
			index       => $secondary_index,
			search_type => 'scan'          # important
		);
	}
	elsif ($load_type == 2) {
	
		$primary_index=shift;
		$primary_field=shift;
		$secondary_index=shift;
		$secondary_field=shift;
		$merge_index=shift;
		$primary_timestamp=shift;
		$secondary_timestamp=shift;
		
		%primary_hash=GetHashValueForIL($primary_index,$primary_field,$primary_timestamp);
		
		%rprimary_hash = reverse %primary_hash;
		
		$hashsecondary_index = $es->scroll_helper(
			index	=> $secondary_index,
			search_type => 'scan',          # important
			body 	=> {
			query => {
				"constant_score" => {
					"filter" => {
						"range" => {
						'@timestamp' => { 
							"gte" => "$secondary_timestamp"
								}
							}			
						}
					}	
				}
			}
		);
	}
	elsif ($load_type==3) {
	
		$primary_index=shift;
		$primary_field=shift;
		$secondary_index=shift;
		$secondary_field=shift;
		$merge_index=shift;
		$primary_from_ts=shift;
		$primary_to_ts=shift;
		$secondary_from_ts=shift;
		$secondary_to_ts=shift;
		
		%primary_hash=GetHashValueForTL($primary_index,$primary_field,$primary_from_ts,$primary_to_ts);
	
		%rprimary_hash = reverse %primary_hash;
		
		$hashsecondary_index = $es->scroll_helper(
			index	=> $secondary_index,
			search_type => 'scan',          # important
			);
	}
		
	while ($result = $hashsecondary_index->next) {
	
		if (exists $rprimary_hash{$$result{'_source'}{$secondary_field}}) {
			$primary_doc = $es->get(
				index   => "$primary_index",
				type    => 'logs',
				id      => "$rprimary_hash{$$result{'_source'}{$secondary_field}}"
			);
			
			$$primary_doc{'_source'}{'@primary_timestamp'} = delete $$primary_doc{'_source'}{'@timestamp'}; #Rename duplicate key @timestamp in primary and secondary index.
			$$result{'_source'}{'@secondary_timestamp'} = delete $$result{'_source'}{'@timestamp'};
			
			%merged = ();
			while ( ($k,$v) = each(%{$$primary_doc{'_source'}}) ) {
				if (exists $result{'_source'}{$k}) {
					warn "Key Exists in Secondary index";
					exit;
				}
				$merged{$k} = $v;
			}
			while ( ($k,$v) = each(%{$$result{'_source'}}) ) {
				if (exists $primary_doc{'_source'}{$k}) {
					warn "Key Exists in Primary index";
					exit;
				}
				$merged{$k} = $v;
			}
			
			$_ //= "null" for values %merged;
			
			$es->index(
				index   => "$merge_index",
				type    => 'logs',
				body    => \%merged
			);
		}
	}
	print "Process Completed. Have a nice day :)\n";
}

sub MergeIndex2 {			#To merge data of 2 index into 1.

print "Loading Data Into Elasticsearch, please note this may take some time...\n";
$load_type=shift;
	if($load_type==1) {			#This section of code is for Full Data load.
		$primary_index=shift;
		$primary_field=shift;
		$secondary_index=shift;
		$secondary_field=shift;
		$merge_index=shift;
		$search=shift;
		
		if ($search==1) {		#This enables search on primary index (with 1 search field, and wildcards are accepted on analyzed field"
			$search_field=shift;
			$search_term=shift;
			$hashprimary_index = $es->scroll_helper(
				index => $primary_index,
				scroll => '60m',
				search_type => 'scan',          #important
				body => {
					query => {
						query_string => { 
							default_field => "$search_field",
							query => "$search_term"
						}
					}
				}
			);
		}
		else {				#To search the whole index.
			$hashprimary_index = $es->scroll_helper(
				index       => $primary_index,
				scroll	    => '60m',
				search_type => 'scan'          #i important
			);
		}
		
		while ($result_pi = $hashprimary_index->next) {		#This loop takes one by record and perform the search with result of primary field in secondary index.
			$$result_pi{'_source'}{'@primary_timestamp'} = delete $$result_pi{'_source'}{'@timestamp'};		#To rename the key of hash from @timestamp to @primary_timestamp,, this helps in avoiding clash in merge hash.
			
			$searchsecondary_index = $es->scroll_helper(		#To get data from secondary index by searching the result of primary field from primary index
				index	=> $secondary_index,
				scroll	=> '60m',
				body 	=> {
					query	=> {
						match	=> {
							$secondary_field	=> {
								query	=> "$$result_pi{'_source'}{$primary_field}",
								type	=> "phrase"
							}
						}
					}
				}
			);
			
			next if ($searchsecondary_index->total == 0);
			
			while ($result_si = $searchsecondary_index->next) {		#This loops takes result of secondary index one by one and merge it with result of primary index and put the index in ES
				$$result_si{'_source'}{'@secondary_timestamp'} = delete $$result_si{'_source'}{'@timestamp'};  #To rename the key of hash from @timestamp to @secondary_timestamp, this helps in avoiding clash in merge hash.
				%merged = undef;	#Flush the merge hash at each iteration 
				while ( ($k,$v) = each(%{$$result_pi{'_source'}}) ) {		#To merge index in merged hash.
					if (exists $result_si{'_source'}{$k}) {
						warn "Key Exists in Secondary index";
						exit;
					}
					$merged{$k} = $v;
				}
				while ( ($k,$v) = each(%{$$result_si{'_source'}}) ) {
					if (exists $result_pi{'_source'}{$k}) {
						warn "Key Exists in Primary index";
						exit;
					}
					$merged{$k} = $v;
				}
				
				$_ //= "null" for values %merged;			#To give "null" values to undefined keys in hash.
				
				$es->index(
					index   => "$merge_index",
					type    => 'logs',
					body    => \%merged
				);
			}
		}
	}
	
	elsif ($load_type==2) {		#Incremental Load
		$primary_index=shift;
		$primary_field=shift;
		$secondary_index=shift;
		$secondary_field=shift;
		$merge_index=shift;
		$primary_timestamp=shift;
		$search=shift;
			
		if ($search==1) {		#Enables search in Incremental Load.
			$search_field=shift;
			$search_term=shift;
			$hashprimary_index = $es->scroll_helper(
				index => $primary_index,
				scroll => '60m',
				search_type => 'scan',          #important
				body => {
					bool => {					#Bool is used to perform multiple query.
						must => [
							{
								"constant_score" => {		#To get data from last inserted data till date.
									"filter" => {
										"range" => {
											"gt" => "$primary_timestamp"		#Timestamp of Last inserted data.
										}
									}
								}
							},
						query_string => { 
							default_field => "$search_field",
							query => "$search_term"
							}
						]
					}
				}
			);
		}
		else {
			$hash_index = $es->scroll_helper(		#Same as above without help.
				index	=> $index,
				search_type => 'scan',          # important
				body 	=> {
					query => {
						"constant_score" => {
							"filter" => {
								"range" => {
									'@timestamp' => { 
									"gte" => "$primary_timestamp"
									}
								}			
							}
						}	
					}
				}
			);
		}
		
		while ($result_pi = $hashprimary_index->next) { #This loop takes one by record and perform the search with result of primary field in secondary index.
			$$result_pi{'_source'}{'@primary_timestamp'} = delete $$result_pi{'_source'}{'@timestamp'};
			
			$searchsecondary_index = $es->scroll_helper(
				index	=> $secondary_index,
				scroll	=> '60m',
				body 	=> {
					query	=> {
						match	=> {
							$secondary_field	=> {
								query	=> "$$result_pi{'_source'}{$primary_field}",
								type	=> "phrase"
							}
						}
					}
				}
			);
			
			next if ($searchsecondary_index->total == 0);
			
			while ($result_si = $searchsecondary_index->next) {
				$$result_si{'_source'}{'@secondary_timestamp'} = delete $$result_si{'_source'}{'@timestamp'};
				%merged = undef;
				while ( ($k,$v) = each(%{$$result_pi{'_source'}}) ) {
					if (exists $result_si{'_source'}{$k}) {
						warn "Key Exists in Secondary index";
						exit;
					}
					$merged{$k} = $v;
				}
				while ( ($k,$v) = each(%{$$result_si{'_source'}}) ) {
					if (exists $result_pi{'_source'}{$k}) {
						warn "Key Exists in Primary index";
						exit;
					}
					$merged{$k} = $v;
				}
				
				$_ //= "null" for values %merged;
				
				$es->index(
					index   => "$merge_index",
					type    => 'logs',
					body    => \%merged
				);
			}
		}
	}
	
	elsif ($load_type==3) {
	
		$primary_index=shift;
		$primary_field=shift;
		$secondary_index=shift;
		$secondary_field=shift;
		$merge_index=shift;
		$primary_from_ts=shift;
		$primary_to_ts=shift;
		$secondary_from_ts=shift;
		$secondary_to_ts=shift;
		$search=shift;
		
		if ($search==1) {
			$search_field=shift;
			$search_term=shift;
			$hashprimary_index = $es->scroll_helper(
				index       => $primary_index,
				scroll	    => '60m',
				search_type => 'scan',          #important
				body		=> {
					query => {
						bool => {
							must => [
								{
									"constant_score" => {
										"filter" => {
											"range" => {
												'@timestamp' => {
													"gt" => "$primary_from_ts",
													"lt" => "$primary_to_ts"
													}
												}
											}
										}
									},
									{
										"query_string" => {
											"default_field" => "$search_field",
											"query" => "$search_term"
										}
									},
									{											#This Bracket is only for pflow-* index. Need to make it generic
										match => {
											"type" => "proftp"
										}
									}
								],
								"must_not" => [									#This bracket is only for pflow-* index. Need to make it generic
									{
										"query_string" => {
											"default_field" => "$primary_field",
											"query" => "dca-*"
										}
									}
								]
							}
						}
					}
				);
		}
		else {
			$hashprimary_index = $es->scroll_helper(
				index       => $primary_index,
				scroll	    => '60m',
				search_type => 'scan',          #important
				body		=> {
					query => {
						constant_score => {
							filter => {
								range => {
								'@timestamp' => {
									gte => "$primary_to_ts",
									lte => "$primary_from_ts"
									}
								}
							}
						}
					}
				}
			);
		}
		while ($result_pi = $hashprimary_index->next) {
			$$result_pi{'_source'}{'@primary_timestamp'} = delete $$result_pi{'_source'}{'@timestamp'};
			
			$searchsecondary_index = $es->scroll_helper(
				index	=> $secondary_index,
				scroll	=> '60m',
				body 	=> {
					"query" => {
						"bool" => {
							"must" => [
								{
									"constant_score" => {
										"filter" => {
											"range" => {
												'@timestamp' => {
													"gt" => "$secondary_from_ts",
													"lt" => "$secondary_to_ts"
												}
											}
										}
									}
								},
								{
									"match" => {
										"$secondary_field" => {
											"query" => "$$result_pi{'_source'}{$primary_field}",
											"type" => "phrase"
										}
									}
								}
							]
						}
					}
				}
			);
			
			next if ($searchsecondary_index->total == 0);
			
			while ($result_si = $searchsecondary_index->next) {
				$$result_si{'_source'}{'@secondary_timestamp'} = delete $$result_si{'_source'}{'@timestamp'};
				%merged = undef;
				while ( ($k,$v) = each(%{$$result_pi{'_source'}}) ) {
					if (exists $result_si{'_source'}{$k}) {
						warn "Key Exists in Secondary index";
						exit;
					}
					$merged{$k} = $v;
				}
				while ( ($k,$v) = each(%{$$result_si{'_source'}}) ) {
					if (exists $result_pi{'_source'}{$k}) {
						warn "Key Exists in Primary index";
						exit;
					}
					$merged{$k} = $v;
				}
				
				$_ //= "null" for values %merged;
				
				$es->index(
					index   => "$merge_index",
					type    => 'logs',
					body    => \%merged
				);
			}
		}
		
	}	
}
	
	
	
{#Inputs

############################ Get Inputs ############################;

print "Enter the hostname: " if ($MANUAL);
$host=<> if ($MANUAL);
chomp $host if ($MANUAL);
$host="162.44.14.70" if ($DEV);
print "ES host is $host\n" if (! $MANUAL);

print "Enter the port number: " if ($MANUAL);
$port=<> if ($MANUAL);
chomp $port if ($MANUAL);
$port="8080" if ($DEV);
print "ES Port is $port\n" if (! $MANUAL);

CheckES($host,$port);

#Initialize Search Elasticsearch
$es = Search::Elasticsearch->new(
    nodes    => ["$host:$port"]
);

print "Enter the name of Primary Index: " if ($MANUAL);
$primary_index=<> if ($MANUAL);
chomp $primary_index if ($MANUAL);
$primary_index="pflow-*" if ($DEV);
print "Primary index is $primary_index \n" if (! $MANUAL);

CheckIfIndexExist($primary_index,0);

print "Enter the name of Field of $primary_index you want to join: " if ($MANUAL);
$primary_field=<> if ($MANUAL);
chomp $primary_field if ($MANUAL);
$primary_field="filename" if ($DEV);
print "Primary Field is $primary_field \n" if (! $MANUAL);

CheckIfFieldExist($primary_index,$primary_field);

print "Would you like to perform search in primary index? [yes] or [no]: " if ($MANUAL);
$perform_search=<> if ($MANUAL);
chomp $perform_search if ($MANUAL);
$perform_search="yes" if ($DEV);
print "Perform Search is $perform_search\n" if ($DEV);
$search=0;
if ($perform_search eq "yes") {
	$search=1;
	print "Enter the field name you want to search: " if ($MANUAL);
	$search_field=<>;
	chomp $search_field;
	$search_field="account_id" if ($DEV);
	print "Search Field is $search_field\n" if ($DEV);
	CheckIfFieldExist($primary_index,$search_field);
	print "Enter the search term you would like to perform (Example, IE*): " if ($MANUAL);
	$search_term=<> if ($MANUAL);
	chomp $search_term if ($MANUAL);
	$search_term="IESOTCH*" if ($DEV);
	print "Search Term is $search_term\n" if ($DEV);
}

print "Enter the name of Secondary Index: " if ($MANUAL);
$secondary_index=<> if ($MANUAL);
chomp $secondary_index if ($MANUAL);
$secondary_index="prod-dcalogs-v0.1" if ($DEV);
print "Secondary index is $secondary_index\n" if (! $MANUAL);

CheckIfIndexExist($secondary_index,0);

print "Enter the name of Field of $secondary_index you want to join: " if ($MANUAL);
$secondary_field=<> if ($MANUAL);
chomp $secondary_field if ($MANUAL);
$secondary_field="sourceFile" if ($DEV);
print "Secondary Field is $secondary_field\n" if (! $MANUAL);

CheckIfFieldExist($secondary_index,$secondary_field);

print "Enter the name of index you want to merge it with: " if ($MANUAL);
$merge_index=<> if ($MANUAL);
chomp $merge_index if ($MANUAL);
$merge_index="merge-pflow-dca" if ($DEV);
print "Merge Index is $merge_index\n" if (! $MANUAL);

CheckIfIndexExist($merge_index,1);

print "Enter type load merge; 1. Full Load 2. Incremental 3. With Time Stamp: " if ($MANUAL);
$load_type=<> if ($MANUAL);
chomp $load_type;
$load_type="3" if ($DEV);
print "Type of Load is $load_type\n" if (! $MANUAL);

## Full Load or Incremental or Time Period.

}

if ($load_type==1) {
	CheckIndexCount($load_type,$primary_index);
	if ($search==1){
		$starttime1 = Benchmark->new;
		MergeIndex2($load_type,$primary_index,$primary_field,$secondary_index,$secondary_field,$merge_index,$search,$search_field,$search_term);
		$finishtime1 = Benchmark->new;
	}
	else {
		$starttime1 = Benchmark->new;
		MergeIndex2($load_type,$primary_index,$primary_field,$secondary_index,$secondary_field,$merge_index,$search);
		$finishtime1 = Benchmark->new;
	}
	print "Total time taken for Merge Process: ". timestr(timediff($finishtime1,$starttime1))."\n";
}
elsif ($load_type==2) {
	if ($merge) {
		print "Index $merge_index is not created, hence a Full Load will happen for the first time\n";
		MergeIndex(1,$primary_index,$primary_field,$secondary_index,$secondary_field,$merge_index);
	}
	elsif (CheckIfIndexEmpty($merge_index)) {
		print "Index $merge_index is empty, hence a Full Load will happen for the first time\n";
		MergeIndex(1,$primary_index,$primary_field,$secondary_index,$secondary_field,$merge_index);
	}
	else {
		$search_primary = $es->search(
			index	=> "$merge_index",
			type	=> 'logs',
			size	=> 1,
			body	=> 	{ 
				query => {
					match_all => { }
				},
				sort => {
					'@primary_timestamp' =>'desc'
				}
			}
		);
		
		$primary_timestamp=$search_primary->{'hits'}->{'hits'}->['']->{'_source'}->{'@primary_timestamp'};
		
		# $search_secondary = $es->search(
			# index	=> "$merge_index",
			# type	=> 'logs',
			# size	=> 1,
			# body	=> 	{ 
				# query => {
					# match_all => { }
				# },
				# sort => {
					# '@secondary_timestamp' =>'desc'
				# }
			# }
		# );
		#$secondary_timestamp=$search_secondary->{'hits'}->{'hits'}->['']->{'_source'}->{'@secondary_timestamp'};
		MergeIndex2($load_type,$primary_index,$primary_field,$secondary_index,$secondary_field,$merge_index,$primary_timestamp);
	}
}

elsif ($load_type==3) {

	print "Please Enter \"From\" TimeStamp for $primary_index in format yyyy-mm-dd HH:MM:SS.ZZZ: " if ($MANUAL);
	$primary_from_ts=<> if ($MANUAL);
	chomp $primary_from_ts if ($MANUAL);
	$primary_from_ts="2016-10-19 12:08:09.419" if ($DEV);
	print "Primary From Time is $primary_from_ts\n" if (! $MANUAL);
	$primary_from_ts =~ s/\s/T/;
	$primary_from_ts =~ s/$/Z/;
	
	$primary_from_ts=ValidateTimeStamp($primary_from_ts);
	
	print "Please Enter \"To\" TimeStamp for $primary_index in format yyyy-mm-dd HH:MM:SS.ZZZ: " if ($MANUAL);
	$primary_to_ts=<> if($MANUAL);
	chomp $primary_to_ts if($MANUAL);
	$primary_to_ts="2016-10-20 12:08:17.483" if ($DEV);
	print "Primary To Time is $primary_to_ts\n" if (! $MANUAL);
	$primary_to_ts =~ s/\s/T/;
	$primary_to_ts =~ s/$/Z/;
	
	$primary_to_ts=ValidateTimeStamp($primary_to_ts);
	
	print "Please Enter \"From\" TimeStamp for $secondary_index in format yyyy-mm-dd HH:MM:SS.ZZZ: " if ($MANUAL);
	$secondary_from_ts=<> if($MANUAL);
	chomp $secondary_from_ts if($MANUAL);
	$secondary_from_ts="2016-10-19 12:08:09.419" if ($DEV);
	print "Secondary From Time is $secondary_from_ts\n" if (! $MANUAL);
	$secondary_from_ts =~ s/\s/T/ ;
	$secondary_from_ts =~ s/$/Z/;
	
	$secondary_from_ts=ValidateTimeStamp($secondary_from_ts);
	
	print "Please Enter \"To\" TimeStamp for $secondary_index in format yyyy-mm-dd HH:MM:SS.ZZZ: " if($MANUAL); 
	$secondary_to_ts=<> if($MANUAL); 
	chomp $secondary_to_ts if($MANUAL);
	$secondary_to_ts="2016-10-20 12:08:17.483" if($DEV);
	print "Secondary To Time is $secondary_to_ts\n" if (! $MANUAL);
	$secondary_to_ts =~ s/\s/T/;
	$secondary_to_ts =~ s/$/Z/;
	
	$secondary_to_ts=ValidateTimeStamp($secondary_to_ts);
	
	CheckIfIndexExist($merge_index);
	if ($search == 1) {
		MergeIndex2($load_type,$primary_index,$primary_field,$secondary_index,$secondary_field,$merge_index,$primary_from_ts,$primary_to_ts,$secondary_from_ts,$secondary_to_ts,$search,$search_field,$search_term);
	}
	else {
		MergeIndex2($load_type,$primary_index,$primary_field,$secondary_index,$secondary_field,$merge_index,$primary_from_ts,$primary_to_ts,$secondary_from_ts,$secondary_to_ts,$search);
	}
}

format HELP = 

========================================================================================================================


@||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
"****************** Developed By Himanshu Ganatra (Developer - Bangalore) ******************"



@||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
"merge_es component is useful for merging two index of Elasticsearch in the same host. There are "
@||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
"two ways to run this component 1st by passing arguments on command line as show below And another"
@||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
'is by passing a parameter "-manual". -manual will ask each and every required fields for merging as an input'
@||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
'If you have any question or suggestion, please feel free to contact me @ HRGanatra@in.imshealth.com'

@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"", "Usage ./merge_es","" , "[ARGUMENTS] OR -manual"


@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"", "[ARGUMENTS]","For load_type=1 or 2","[load_type] [host] [port] [primary_index] [primary_field] "
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","[secondary_index] [secondary_field] [merge_index]"


@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"", "[ARGUMENTS]","For load_type=3","[load_type] [host] [port] [primary_index] [primary_field]"
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","[primary_from_timestamp] [primary_to_timestamp] [secondary_index] "
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","[secondary_field] [secondary_from_timestamp] [secondary_to_timestamp]"
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","[merge_index]"


@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","[ARGUMENTS]","Example for load_type=1 or 2","./merge_es 1 localhost 9200 test-panel-data "
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","organisationExternalIdentifier test-file-processing"
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","imsSupplierIdentifier test-panel-file-data"


@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","[ARGUMENTS]","Example for load_type=3","./merge_es 1 localhost 9200 test-panel-data "
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","organisationExternalIdentifier \"'2016-09-30 15:14:21.123\' "
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","\'2016-10-21 22:51:21.123\' test-file-processing"
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","imsSupplierIdentifier \'2016-09-30 15:14:21.123\' \'2016-10-21 22:51:21.123\' "
@<<<<<< @<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
"","","","test-panel-file-data"

========================================================================================================================
.
